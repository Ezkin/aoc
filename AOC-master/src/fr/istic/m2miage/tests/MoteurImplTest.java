package fr.istic.m2miage.tests;

import fr.istic.m2miage.metronome.*;
import junit.framework.TestCase;

import static org.mockito.Mockito.*;

/**
 * Created by epeti on 09/01/2016.
 */
public class MoteurImplTest extends TestCase {

    MoteurImpl _moteur;
    public void setUp() throws Exception {
        super.setUp();

        _moteur = new MoteurImpl();

        _moteur.addCommand(Signals.MarkMesure, mock(CommandControlleur.class));
        _moteur.addCommand(Signals.MarkTempo, mock(CommandControlleur.class));
        _moteur.addCommand( Signals.UpdateMesure, mock(CommandControlleur.class));

    }

    public void tearDown() throws Exception {

    }

    public void testStartMoteur() throws Exception {

        _moteur.startMoteur();
        assertTrue("Tempo is not reset", _moteur.getNbTemps() == 0);

        assertTrue(_moteur.getTimer().getListeTimers().containsKey(_moteur.getMoteurTickCommandControlleur()));

    }

    public void testTimerTick() throws Exception {

        //do timertick
        int lastValue = _moteur.getNbTemps();
        _moteur.timerTick();

        //chek that the mark tempo is always called
        verify(_moteur.commands.get(Signals.MarkTempo), times(1)).execute();
        verify(_moteur.commands.get(Signals.MarkMesure), times(1)).execute();


        //check that nbTemps is updated every timertick
        assertTrue("nbTemps is not updated", lastValue + 1 == _moteur.getNbTemps());


        //do a second timertick
        lastValue = _moteur.getNbTemps();
        _moteur.timerTick();

        //a new marktempo has been done
        verify(_moteur.commands.get(Signals.MarkTempo), times(2)).execute();

        //this time, no mark mesure done
        verify(_moteur.commands.get(Signals.MarkMesure), times(1)).execute();

    }

    public void testStopMoteur() throws Exception {

        if(_moteur.isActive()){
            _moteur.startMoteur();
        }

        //Check that the timer contains the command
        if(_moteur.isActive()) {
            assertTrue(_moteur.getTimer().getListeTimers().containsKey(_moteur.getMoteurTickCommandControlleur()));


            _moteur.stopMoteur();

            //check that the command is removed from the timer
            assertTrue(!_moteur.getTimer().getListeTimers().containsKey(_moteur.getMoteurTickCommandControlleur()));

        }
    }

    public void testIncMesure() throws Exception {
        _moteur.setMesure(MoteurImpl.MIN_MESURE);

        //check command is called
        verify(_moteur.commands.get(Signals.UpdateMesure), times(1));

        int cur = _moteur.getMesure();

        //do inc
        _moteur.incMesure();

        //check mesure value is increased by 1
        assertTrue("Mesure not incremented", _moteur.getMesure() == cur + 1);


        //check command is called when inc is done
        verify(_moteur.commands.get(Signals.UpdateMesure), times(1));


        //check mesure dont go over limitation
        int t =  MoteurImpl.MAX_MESURE + 1;
        for (int i = 0; i < t; i++) {
            _moteur.incMesure();
        }

        assertEquals("Mesure goes over limitation", _moteur.getMesure(), MoteurImpl.MAX_MESURE);

    }

    public void testDecMesure() throws Exception {
        _moteur.setMesure(MoteurImpl.MAX_MESURE);
        //check command is called
        verify(_moteur.commands.get(Signals.UpdateMesure), times(1));

        int last = _moteur.getMesure();

        //do dec
        _moteur.decMesure();

        //check mesure value is decreased by one
        assertTrue("Mesure not decremented", _moteur.getMesure() == last - 1 );

        //check command is called when inc is done
        verify(_moteur.commands.get(Signals.UpdateMesure), times(1));


        //check mesure dont go under limitation
        int t =  MoteurImpl.MAX_MESURE + 10;
        for (int i = 0; i < t; i++) {
            _moteur.decMesure();
        }
        assertEquals("Mesure goes under limitation", _moteur.getMesure(), MoteurImpl.MIN_MESURE);
    }
}