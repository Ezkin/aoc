package fr.istic.m2miage.tests;

import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import org.junit.*;

import fr.istic.m2miage.metronome.*;
import static org.junit.Assert.*;

/**
 * The class <code>ControllerTest</code> contains tests for the class <code>{@link Controller}</code>.
 *
 * @generatedBy CodePro at 23/10/15 08:58
 * @author fesnault
 * @version $Revision: 1.0 $
 */
public class ControllerTest {
	
	IHM ihm;
	Controller controlleur;
	Moteur moteur;
	
	
	
	/**
	 * Perform pre-test initialization.
	 *
	
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 * @throws Exception
	 *         if the initialization fails for some reason */
	@Before
	public void setUp()
		throws Exception {
		
		// To remove "Toolkit not initialized" error
		new JFXPanel();
		
		moteur = new MoteurImpl();
    	ihm = new IHMAdapter();
    	controlleur = new Controller(moteur, ihm);
    	
    	FXMLLoader loader = new FXMLLoader();
    	
    	loader.setLocation(Metronome.class.getResource("Metronome.fxml"));
    	
    	loader.setController(ihm);
    	Parent root = loader.<Parent>load();
	}
	
	/**
	 * Run the Controller(Moteur) constructor test.
	 *
	
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 * @throws Exception */
	@Test
	public void testController()
		throws Exception {
		assertNotNull(controlleur);
	}

	/**
	 * Run the void dec() method test.
	 *
	
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 * @throws Exception */
	@Test
	public void testDec()
		throws Exception {

		assertEquals("Mesure initiale à 2", controlleur.getMoteur().getMesure(), 2);
		controlleur.dec();
		assertEquals("Mesure à 2 car mesure minimale est 2", controlleur.getMoteur().getMesure(), 2);
	}

	/**
	 * Run the void inc() method test.
	 *
	
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 * @throws Exception */
	@Test
	public void testInc()
		throws Exception {

		assertEquals("Mesure initiale à 2", controlleur.getMoteur().getMesure(), 2);
		controlleur.inc();
		assertEquals("Mesure à 3 après inc", controlleur.getMoteur().getMesure(), 3);
		controlleur.inc();
		assertEquals("Mesure à 4 après inc", controlleur.getMoteur().getMesure(), 4);
		controlleur.inc();
		controlleur.inc();
		controlleur.inc();
		assertEquals("Mesure à 7 après inc", controlleur.getMoteur().getMesure(), 7);
		controlleur.inc();
		assertEquals("Mesure à 7 car mesure maximale est 7", controlleur.getMoteur().getMesure(), 7);
	}

	/**
	 * Run the void start() method test.
	 *
	
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 * @throws Exception */
	@Test
	public void testStart()
		throws Exception {

		assertFalse("Moteur éteint", controlleur.getMoteur().isActive());
		controlleur.start();
		assertTrue("Moteur allumé après start", controlleur.getMoteur().isActive());
		controlleur.start();
		assertTrue("Moteur allumé après start", controlleur.getMoteur().isActive());
	}

	/**
	 * Run the void stop() method test.
	 *
	
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 * @throws Exception */
	@Test
	public void testStop()
		throws Exception {

		assertFalse("Moteur éteint", controlleur.getMoteur().isActive());
		controlleur.start();
		assertTrue("Moteur allumé après start", controlleur.getMoteur().isActive());
		controlleur.stop();
		assertFalse("Moteur éteint après stop", controlleur.getMoteur().isActive());
		controlleur.stop();
		assertFalse("Moteur éteint après stop", controlleur.getMoteur().isActive());
	}


	/**
	 * Run the void updateSlider() method test.
	 *
	
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 * @throws Exception */
	@Test
	public void testUpdateSlider()
		throws Exception {
		assertEquals("Valeur tempo (moteur) égale à 100 après initialisation", moteur.getTempo(), 100);
		assertEquals("Valeur tempo (ihm) égale à 100 après initialisation", ihm.getSlider().getValue(), 100, 0.1);
		controlleur.updateSlider(200);
		
		assertEquals("Valeur tempo égale à 200 après changement de valeur", moteur.getTempo(), 200);
		assertEquals("Valeur tempo (ihm) à 200 après changement de valeur", ihm.getLabel().getText(), "200");
		
		controlleur.updateSlider(50);
		assertEquals("Valeur tempo égale à 50 après changement de valeur", moteur.getTempo(), 50);
		assertEquals("Valeur tempo (ihm) à 50 après changement de valeur", ihm.getLabel().getText(), "50");
	}

	

	/**
	 * Perform post-test clean-up.
	 *
	
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 * @throws Exception
	 *         if the clean-up fails for some reason */
	@After
	public void tearDown()
		throws Exception {
		
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 23/10/15 08:58
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ControllerTest.class);
	}
}