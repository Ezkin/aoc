package fr.istic.m2miage.tests;

import fr.istic.m2miage.metronome.*;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by epeti on 09/01/2016.
 */
public class ButtonAdapterTest extends TestCase {

    IHM ihm;
    Controller controlleur;
    Moteur moteur;

    @Mock
    private ButtonAdapter btnAdapt;

    @Before
    public void setUp() throws Exception {

        // To remove "Toolkit not initialized" error
        new JFXPanel();

        moteur = new MoteurImpl();
        ihm = new IHMAdapter();
        controlleur = new Controller(moteur, ihm);

        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Metronome.class.getResource("Metronome.fxml"));

        loader.setController(ihm);
        Parent root = loader.<Parent>load();

    }

    @Test
    public void testPoll() throws Exception {

        IHMAdapter adapt = mock(IHMAdapter.class);

        btnAdapt = new ButtonAdapter(adapt, 0, mock(CommandControlleur.class) );

        //since the button is not pressed, the command should not be called
        verify(btnAdapt.getCmd(), times(0)).execute();


        //every 100 ms we check the button state so after 1s the check will be done 10 times
        verify(btnAdapt.getClavier(), timeout(1000).atLeast(10)).touchePressee(btnAdapt.getToucheId());

    }
}