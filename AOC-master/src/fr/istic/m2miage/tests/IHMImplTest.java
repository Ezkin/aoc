package fr.istic.m2miage.tests;

import fr.istic.m2miage.metronome.*;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by epeti on 09/01/2016.
 */
public class IHMImplTest extends TestCase {

    IHM ihm;
    Controller controlleur;
    Moteur moteur;

    IHMImpl imhTest;

    @Before
    public void setUp() throws Exception {

        // To remove "Toolkit not initialized" error
        new JFXPanel();

        moteur = new MoteurImpl();
        ihm = new IHMImpl();
        controlleur = new Controller(moteur, ihm);

        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Metronome.class.getResource("Metronome.fxml"));

        loader.setController(ihm);
        Parent root = loader.<Parent>load();


        imhTest = new IHMImpl();

        imhTest.setController(mock(Controller.class));
        imhTest.setSound(mock(SoundEmmitter.class));

    }

    @Test
    public void testStart() throws Exception {

        //do
        imhTest.start();

        //check start is called on controlleur
        verify(imhTest.getController()).start();

    }

    @Test
    public void testStop() throws Exception {

        //do stop
        imhTest.stop();

        //check stop is called on controlleur
        verify(imhTest.getController()).stop();
    }

    @Test
    public void testInc() throws Exception {

        //do inc
        imhTest.inc();

        //check inc is called on controlleur
        verify(imhTest.getController()).inc();
    }

    @Test
    public void testDec() throws Exception {

        //do dec
        imhTest.dec();

        //check dec is called on controlleur
        verify(imhTest.getController()).dec();
    }

    @Test
    public void testUpdateSlider() throws Exception {

        //cannot be test need a slider but it's a javaFX element
        /*
        //do dec
        imhTest.updateSlider();

        //check dec is called on controlleur
        verify(imhTest.getController()).updateSlider(anyInt());
        */
    }

    @Test
    public void testMarkTempo() throws Exception {


        //do markTempo
        imhTest.markTempo();

        //check sound is played on tempo
        verify(imhTest.getSound()).playSound();
    }

    @Test
    public void testMarkMesure() throws Exception {

        //do markMesure
        imhTest.markMesure();

        //check sound is NOT played on Mesure
        verify(imhTest.getSound(), never()).playSound();
    }

    @Test
    public void testUpdateTempo() throws Exception {
        //nothing to test
    }
}