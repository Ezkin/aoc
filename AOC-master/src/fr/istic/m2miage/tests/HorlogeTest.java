package fr.istic.m2miage.tests;

import fr.istic.m2miage.metronome.CommandMoteur;
import fr.istic.m2miage.metronome.Horloge;
import fr.istic.m2miage.metronome.SuperCommand;
import javafx.embed.swing.JFXPanel;
import junit.framework.TestCase;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.swing.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;

/**
 * Created by epeti on 09/01/2016.
 */
public class HorlogeTest extends TestCase {

    Horloge h;

    public void setUp() throws Exception {
        super.setUp();


        h = new Horloge();

        //check that the timer list is properly initialized
        assertTrue(h.getListeTimers() != null);
        assertTrue(h.getListeTimers().size() == 0);
    }

    public void tearDown() throws Exception {

    }

    @Test
    public void testActiverPeriodiquement() throws Exception {

        //Create a command
        SuperCommand cmdMock1 = mock(CommandMoteur.class);
        SuperCommand cmdMock2 = mock(CommandMoteur.class);
        SuperCommand cmdMock3 = mock(CommandMoteur.class);

        //enable the command
        h.activerPeriodiquement(cmdMock1, 1000);
        h.activerPeriodiquement(cmdMock2, 2000);
        h.activerPeriodiquement(cmdMock3, 3000);

        //check that the timer list contains the selected command
        assertTrue(h.getListeTimers().containsKey(cmdMock1));
        assertTrue(h.getListeTimers().containsKey(cmdMock2));
        assertTrue(h.getListeTimers().containsKey(cmdMock3));

        assertTrue(h.getListeTimers().size() == 3);

        //the command is called

        //verify(cmdMock1,timeout(5000).times(5)).execute();
    }

    @Test
    public void testDesactiver() throws Exception {

        //mock command
        SuperCommand cmdMock = mock(CommandMoteur.class);
        SuperCommand cmdMock2 = mock(CommandMoteur.class);

        //add the command
        h.activerPeriodiquement(cmdMock, 1000);
        h.activerPeriodiquement(cmdMock2, 2000);

        //disable it
        h.desactiver(cmdMock);

        //check that the command is removed from the list of commands
        assertTrue(!h.getListeTimers().containsKey(cmdMock));
        assertTrue(h.getListeTimers().size() == 1);
    }
}