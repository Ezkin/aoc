package fr.istic.m2miage.metronome;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class SoundEmmitterImpl implements SoundEmmitter {

    private static MediaPlayer mediaPlayer;

    /**
     * Constructor SoundEmmitterImpl
     * On defini l'URL du son, dans notre cas res/sound.mp3
     * On initialise un nouveau MediaPlayer
     */
    public SoundEmmitterImpl() {
        //tring ssound = "res/sound.mp3";
        Media sound  = new Media(getClass().getResource("res/sound.mp3").toExternalForm());
        mediaPlayer = new MediaPlayer(sound);
    }

    /*
     * Method playSound.
     * @see fr.istic.m2miage.metronome.SoundEmmitter#playSound()
     */
    public void playSound() {
        //new Thread(this).start();

        mediaPlayer.stop();
        mediaPlayer.play();
    }

}
