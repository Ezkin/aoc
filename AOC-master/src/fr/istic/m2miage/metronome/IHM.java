/**
 * 
 */
package fr.istic.m2miage.metronome;

import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.shape.Circle;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface IHM {

	/**
	 * Incrémenter la mesure
	 */
	public abstract void inc();

	/**
	 * Décrémenter la mesure
	 */
	public abstract void dec();

	/**
	 * Marquer la mesure
	 */
	public abstract void markMesure();
	
	/**
	 * Maruer le tempo
	 */
	public abstract void markTempo();

	/**
	 * Method getSlider pour retourner le slider de l'IHM
	 * @return Slider
	 */
	public abstract Slider getSlider();

	/**
	 * Method getLabel pour retourner le label de l'IHM
	 * @return Label
	 */
	public abstract Label getLabel();

	/**
	 * Method getL1 pour retourner la première LED de l'IHM
	 * @return Circle
	 */
	public abstract Circle getL1();

	/**
	 * Method getL2 pour retourner la seconde LED de l'IHM
	 * @return Circle
	 */
	public abstract Circle getL2();

	/**
	 * Method setController pour fixer le controleur à l'IHM
	 * @param controller Controller
	 */
	public abstract void setController(Controller controller);
	
	/**
	 * Method updateTempo pour afficher le tempo sur l'IHM
	 * @param i int
	 */
	public abstract void updateTempo(int i);

	/**
	 * Method updateSlider pour mettre la nouvelle valeur du moteur dans le slider de l'IHM
	 */
	public abstract void updateSlider();
	
}