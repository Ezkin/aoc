package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface CommandControlleur extends SuperCommand {
    /**
     * Method setReciever pour mettre le controlleur en receiver de la commande
     * @param controller Controller
     */
    void setReceiver(Controller controller);
}