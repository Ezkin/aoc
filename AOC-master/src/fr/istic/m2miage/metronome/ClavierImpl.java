/**
 * 
 */
package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class ClavierImpl implements Clavier {

	/*
	 * Method touchePressee.
	 * @param i int
	 * @return boolean
	 * @see fr.istic.m2miage.metronome.Clavier#touchePressee(int)
	 */
	@Override
	public boolean touchePressee(int i) {
		return false;
	}

}
