package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface CommandMoteur extends SuperCommand {
    /**
     * Method setReciever pour mettre le moteur en receiver de la commande
     * @param m Moteur
     */
    void setReceiver(Moteur m);
}