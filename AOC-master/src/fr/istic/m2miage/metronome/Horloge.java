package fr.istic.m2miage.metronome;
import java.util.*;

import javafx.application.Platform;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class Horloge{

    private Map<SuperCommand,Timer> listeTimers;

    public Horloge() {
        setListeTimers(new HashMap<>());
    }

    /**
     * Method activerPeriodiquement pour exécuter une commande tous les tps ms
     * @param cmd SuperCommand n'importe quelle commande
     * @param tps un nombre en ms (100 dans notre cas)
     */
    public void activerPeriodiquement(SuperCommand cmd, long tps){

        //si la commande n'est pas deja enregistree
        if(!getListeTimers().containsKey(cmd)){

            Timer timer = new Timer();

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                	Platform.runLater(() -> {
                		cmd.execute();
                    });
                    
                }
            }, 0, tps);

            getListeTimers().put(cmd, timer);
        }else{
            System.out.println("Commande deja enregistree");
        }
    }

    /**
     * Method activerApresDelai // useless dans notre cas
     * @param cmd CommandMoteur
     * @param delaiEnSecondes float
     */
    public void activerApresDelai(CommandMoteur cmd, float delaiEnSecondes){

    }

    /**
     * Method desactiver pour enlever une commande de la map
     * @param cmd CommandMoteur
     */
    public void desactiver(SuperCommand cmd){
        if(getListeTimers().containsKey(cmd)){
            getListeTimers().get(cmd).cancel();
            getListeTimers().remove(cmd);
        }
    }

    public Map<SuperCommand, Timer> getListeTimers() {
        return listeTimers;
    }

    public void setListeTimers(Map<SuperCommand, Timer> listeTimers) {
        this.listeTimers = listeTimers;
    }
}
