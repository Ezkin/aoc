/**
 * 
 */
package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface CommandPoll extends SuperCommand {
	/**
	 * Method setReciever pour mettre un adapter en receiver de la commande
	 * @param b MyAdapter
	 */
	void setReceiver(MyAdapter b);

}
