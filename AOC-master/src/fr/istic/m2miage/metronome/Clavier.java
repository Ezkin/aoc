/**
 * 
 */
package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface Clavier {
	/**
	 * Method touchePressee retourne true sur le boutin i est enfoncé, sinon false
	 * @param i int (1 start, 2 stop, 3 inc, 4 dec)
	 * @return boolean
	 */
	public boolean touchePressee(int i);
}
