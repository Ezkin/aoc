package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public enum Signals {
    MarkMesure,
    UpdateActive, UpdateMesure, UpdateTempo, MarkTempo
}