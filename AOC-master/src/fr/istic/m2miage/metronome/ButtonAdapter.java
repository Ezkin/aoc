/**
 * 
 */
package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class ButtonAdapter implements MyAdapter {


	private Horloge horloge;
	private CommandPoll poll;
	private Clavier clavier;
	private CommandControlleur cmd;
	private int toucheId;
	
	/**
	 * Constructor for ButtonAdapter.
	 * @param ihm IHMAdapter
	 * @param toucheId int correspondant à la touche pressée toucheId (1 start, 2 stop, 3 inc, 4 dec)
	 * @param cmd CommandControlleur Start, Stop, Inc, Dec
	 */
	public ButtonAdapter(IHMAdapter ihm, int toucheId, CommandControlleur cmd) {
		horloge = new Horloge();
		poll = new Poll();
		this.clavier = ihm;
		this.toucheId = toucheId;
		this.cmd = cmd;
		poll.setReceiver(this);
		horloge.activerPeriodiquement(poll, 100);
	}
	
	/*
	 * Method poll.
	 * @see fr.istic.m2miage.metronome.MyAdapter#poll()
	 */
	@Override
	public void poll() {
		if(clavier.touchePressee(toucheId)) {
			cmd.execute();
		}
	}


	public Horloge getHorloge() {
		return horloge;
	}

	public CommandPoll getPoll() {
		return poll;
	}

	public Clavier getClavier() {
		return clavier;
	}

	public CommandControlleur getCmd() {
		return cmd;
	}

	public int getToucheId() {
		return toucheId;
	}
}
