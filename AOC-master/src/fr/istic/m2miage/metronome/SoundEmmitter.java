package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface SoundEmmitter {
	
	/**
	 * Method playSound pour jouer un son lors du tempo
	 */
    public void playSound();
}
