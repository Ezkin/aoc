package fr.istic.m2miage.metronome;

import java.util.*;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class MoteurImpl implements Moteur {

    public static int MAX_MESURE = 7;
    public static int MIN_MESURE = 2;

    private int nbTemps;

    public Map<Signals,CommandControlleur> commands;

    private CommandMoteur moteurTickCommandControlleur;

    private int tempo;

    private int mesure;

    private Boolean active;

    private Horloge timer;

    /**
     * Default constructor
     */
    public MoteurImpl() {
        active = false;
        commands = new HashMap<Signals,CommandControlleur>();
        mesure = MIN_MESURE;
        tempo = 100;
        timer = new Horloge();
        nbTemps = 0;
    }

    /*
     * Method startMoteur.
     * @see fr.istic.m2miage.metronome.Moteur#startMoteur()
     */
    public void startMoteur(){
        //start
        nbTemps = 0;

        moteurTickCommandControlleur = new CommandMoteur() {
            private Moteur _moteur;

            @Override
            public void execute() {
                _moteur.timerTick();
            }

            @Override
            public void setReceiver(Moteur m) {
                _moteur = m;
            }

        };

        moteurTickCommandControlleur.setReceiver(this);

        //on active le timer tout les "temps" milisecondes
        long tps = 60000/tempo;

        timer.activerPeriodiquement(moteurTickCommandControlleur, tps);
    }

    /*
     * Method timerTick.
     * @see fr.istic.m2miage.metronome.Moteur#timerTick()
     */
    public void timerTick(){
        if((nbTemps % mesure) == 0){
            commands.get(Signals.MarkMesure).execute();
            commands.get(Signals.MarkTempo).execute();
        }else{
            commands.get(Signals.MarkTempo).execute();
        }
        nbTemps++;
    }

    /*
     * Method stopMoteur.
     * @see fr.istic.m2miage.metronome.Moteur#stopMoteur()
     */
    public void stopMoteur(){
        timer.desactiver(moteurTickCommandControlleur);
    }

    /*
    
     * @return int
     * @see fr.istic.m2miage.metronome.Moteur#getTempo()
     */
    public int getTempo() {
        return tempo;
    }

    /*
     * @param t
     * @see fr.istic.m2miage.metronome.Moteur#setTempo(int)
     */
    public void setTempo(int t) {
        if(t != tempo) {
            tempo = t;
            commands.get(Signals.UpdateTempo).execute();
            stopMoteur();

            //restart only if it was on before
            if(isActive())
                startMoteur();

        }

    }

    /*
     * Method updateTempo.
     * @see fr.istic.m2miage.metronome.Moteur#updateTempo()
     */
    public void updateTempo(){

    }

    /*
    
     * @return int
     * @see fr.istic.m2miage.metronome.Moteur#getMesure()
     */
    public int getMesure() {
        return mesure;
    }

    /*
     * @param mesure
     * @see fr.istic.m2miage.metronome.Moteur#setMesure(int)
     */
    public void setMesure(int mesure) {

        if(mesure < MIN_MESURE) mesure = MIN_MESURE;

        if(mesure > MAX_MESURE) mesure = MAX_MESURE;

        this.mesure = mesure;

        commands.get(Signals.UpdateMesure).execute();
    }

    /*
     * Method isActive.
     * @return Boolean
     * @see fr.istic.m2miage.metronome.Moteur#isActive()
     */
    @Override
    public Boolean isActive() {
        return active;
    }

    /*
     * @param nouveauEtat
     * @see fr.istic.m2miage.metronome.Moteur#setActive(Boolean)
     */
    public void setActive(Boolean nouveauEtat) {
        active = nouveauEtat;
        commands.get(Signals.UpdateActive).execute();
    }

    /*
     * @param s 
     * @param cmd
     * @see fr.istic.m2miage.metronome.Moteur#addCommand(Signals, CommandControlleur)
     */
    public void addCommand(Signals s, CommandControlleur cmd) {
        // TODO implement here
        if(!commands.containsKey(s)){
            commands.put(s,cmd);
        }
    }

    /*
     * Method incMesure.
     * @see fr.istic.m2miage.metronome.Moteur#incMesure()
     */
    public void incMesure(){
        setMesure( this.mesure + 1 );
    }

    /*
     * Method decMesure.
     * @see fr.istic.m2miage.metronome.Moteur#decMesure()
     */
    public void decMesure(){
        setMesure( this.mesure - 1 );
    }


    public int getNbTemps() {
        return nbTemps;
    }

    public void setNbTemps(int nbTemps) {
        this.nbTemps = nbTemps;
    }



    public Horloge getTimer() {
        return timer;
    }

    public void setTimer(Horloge timer) {
        this.timer = timer;
    }

    public CommandMoteur getMoteurTickCommandControlleur() {
        return moteurTickCommandControlleur;
    }

    public void setMoteurTickCommandControlleur(CommandMoteur moteurTickCommandControlleur) {
        this.moteurTickCommandControlleur = moteurTickCommandControlleur;
    }



}