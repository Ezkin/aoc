package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface CommandAdapter extends SuperCommand {
    /**
     * Method setReciever pour mettre l'IHM en receiver de la commande
     * @param ihm IHM
     */
    void setReciever(IHM ihm);
}