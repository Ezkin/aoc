package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class Controller {

    private Moteur moteur;
    
    private IHM ihm;

    /**
     * Constructor for Controller.
     * Il créé les commandes du controleur 
     * 	MarquerMesure,
     * 	MarquerTempo
     * 	UpdateActive,
     * 	UpdateMesure, 
     * 	UpdateTempo
     * 
     * @param m Moteur du métronome
     * @param i IHM du métronome
     */
    public Controller(Moteur m, IHM i) {

        moteur = m;
        ihm = i;
        ihm.setController(this);
        //initialisation

        //MARQUER MESURE
        CommandControlleur markMesureCmd = new CommandControlleur() {
            private Controller _ctrl;

            @Override
            public void execute() {
                _ctrl.markMesure();
            }

            @Override
            public void setReceiver(Controller controller) {
                _ctrl = controller;
            }
        };
        markMesureCmd.setReceiver(this);
        moteur.addCommand(Signals.MarkMesure, markMesureCmd);

        //MARQUER TEMPO
        CommandControlleur markTempoCmd = new CommandControlleur() {
            private Controller _ctrl;

            @Override
            public void execute() {
                _ctrl.markTempo();
            }

            @Override
            public void setReceiver(Controller controller) {
                _ctrl = controller;
            }
        };
        markTempoCmd.setReceiver(this);
        moteur.addCommand(Signals.MarkTempo, markTempoCmd);

        //UPDATE EN MARCHE
        CommandControlleur updateActive = new CommandControlleur() {
            private Controller _ctrl;

            @Override
            public void execute() {
                _ctrl.updateActive();
            }

            @Override
            public void setReceiver(Controller controller) {
                _ctrl = controller;
            }
        };
        updateActive.setReceiver(this);
        moteur.addCommand(Signals.UpdateActive, updateActive);


        //UPDATE EN MESURE
        CommandControlleur updateMesure = new CommandControlleur() {
            private Controller _ctrl;

            @Override
            public void execute() {
                _ctrl.updateMesure();
            }

            @Override
            public void setReceiver(Controller controller) {
                _ctrl = controller;
            }
        };
        updateMesure.setReceiver(this);
        moteur.addCommand(Signals.UpdateMesure, updateMesure);

        //UPDATE EN TEMPO
        CommandControlleur updateTempo = new CommandControlleur() {
            private Controller _ctrl;

            @Override
            public void execute() {
                _ctrl.updateTempo();
            }

            @Override
            public void setReceiver(Controller controller) {
                _ctrl = controller;
            }
        };
        updateTempo.setReceiver(this);
        moteur.addCommand(Signals.UpdateTempo, updateTempo);

        //sound
        
    }

    /**
     * Method start pour demander au moteur de démarrer s'il ne l'est pas
     */
    public void start(){
        if(!moteur.isActive()){
            moteur.setActive(true);
        }
    }

    /**
     * Method stop pour demander au moteur de s'éteindre s'il ne l'est pas
     */
    public void stop(){
        if(moteur.isActive()){
            moteur.setActive(false);
        }
    }

    /**
     * Method inc pour demander au moteur d'incrémenter la mesure
     */
    public void inc(){
        moteur.incMesure();
    }

    /**
     * Method dec pour demander au moteur de décrémenter la mesure
     */
    public void dec(){
        moteur.decMesure();
    }

    /**
     * Method updateSlider pour envoyer la nouvelle valeur du tempo au moteur
     * @param value int la valeur du tempo
     */
    public void updateSlider(int value){
        moteur.setTempo(value);
    }

    /**
     * Method updateActive pour notifier que l'état du moteur a changé
     */
    public void updateActive(){
        if(moteur.isActive()){
            //on allume le moteur
            moteur.startMoteur();
        } else {
            //on eteint
            moteur.stopMoteur();
        }
    }

    /**
     * Method updateTempo pour mettre sur l'IHM la nouvelle valeur du tempo du moteur
     */
    public void updateTempo(){
    	ihm.updateTempo(moteur.getTempo());
    }

    /**
     * Method updateMesure // inutile ?
     */
    public void updateMesure(){
        //int value = moteur.getMesure();
        //affichageMesure.setText(value+"");
    }

    /**
     * Method markTempo pour dire à l'IHM de faire les signaux correspondant au tempo
     */
    public void markTempo(){
        ihm.markTempo();
    }

    /**
     * Method markMesure pour dire à l'IHM de faire les signaux correspondant à la mesure
     */
    public void markMesure(){
    	ihm.markMesure();
    }

	/**
	 * Method getMoteur pour récupérer le moteur
	 * @return Moteur
	 */
	public Moteur getMoteur() {
		return moteur;
	}

}
