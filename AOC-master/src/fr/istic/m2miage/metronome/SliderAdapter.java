package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class SliderAdapter implements MyAdapter {
    Horloge h;
    CommandPoll p;
    Molette molette;
    float lastPosition;
    CommandAdapter cmd;

    /**
     * Constructor for SliderAdapter.
     * Initialiser de l'horloge et du poll
     * On recupere la molette et la commande pour envoyer des messages
     * On active l'horloge avec le Poll toutes les 100ms
     * @param ihm IHMAdapter
     * @param updateSlider CommandAdapter
     */
    public SliderAdapter(IHMAdapter ihm, CommandAdapter updateSlider) {
        h = new Horloge();
        p = new Poll();
        this.molette = ihm;
        this.cmd = updateSlider;
        p.setReceiver(this);
        h.activerPeriodiquement(p, 100);
    }

    /*
     * Method poll.
     * @see fr.istic.m2miage.metronome.MyAdapter#poll()
     */
    public void poll() {
        float pos = molette.position();

        if(Math.abs(pos - lastPosition) > 0.01){
            System.out.println(pos + " != "+ lastPosition);
            lastPosition = pos;
            cmd.execute();
        }
    }

}