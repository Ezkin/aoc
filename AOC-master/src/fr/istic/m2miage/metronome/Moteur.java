package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface Moteur {

    /**
     * Method getTempo getter pour recuperer le tempo du moteur
     * @return int entre 30 et 300
     */
    public int getTempo();

    /**
     * Method setTempo setter pour fixer la valeur du tempo du moteur
     * @param t entre 30 et 300
     */
    public void setTempo(int t);

    /**
     * Method getMesure getter pour recuperer la mesure du moteur
     * @return int entre 2 et 7
     */
    public int getMesure();

    /**
     * Method setMesure setter pour fixer la mesure du moteur
     * @param bpm entre 2 et 7
     */
    public void setMesure(int bpm);

    /**
     * Method isActive pour savoir si le moteur est en marche ou non
     * @return Boolean true si en marche, false sinon
     */
    public Boolean isActive();

    /**
     * Method setActive pour mettre en marche ou non le moteur
     * @param b true si en marche, false sinon
     */
    public void setActive(Boolean b);

    /**
     * Method addCommand pour ajouter une commande au moteur
     * @param s dans l'enumeration Signals
     * @param cmd une CommandControlleur pour envoyer les messages futurs
     */
    public void addCommand(Signals s, CommandControlleur cmd);

    /**
     * Method startMoteur pour demarrer le moteur
     */
    public void startMoteur();

    /**
     * Method startMoteur pour éteindre le moteur
     */
    public void stopMoteur();

    /**
     * Method updateTempo // useless ?
     */
    public void updateTempo();

    /**
     * Method timerTick pour envoyer des messages au controleur selon la mesure et tempo
     * Si la valeur du tempo est egale a la valeur de la mesure, alors on marque la mesure
     * Dans tous les cas, on marque le tempo
     */
    public void timerTick();

    /**
     * Method incMesure pour incrementer la mesure (7 maximum)
     */
    void incMesure();

    /**
     * Method decMesure pour decrementer la mesure (2 minimum)
     */
    void decMesure();
}