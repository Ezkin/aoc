/**
 * 
 */
package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class Poll implements CommandPoll {

	MyAdapter b;

	/*
	 * Method execute.
	 * @see fr.istic.m2miage.metronome.SuperCommand#execute()
	 */
	@Override
	public void execute() {
		b.poll();
	}

	/*
	 * Method setReceiver.
	 * @param b MyAdapter
	 * @see fr.istic.m2miage.metronome.CommandPoll#setReceiver(MyAdapter)
	 */
	@Override
	public void setReceiver(MyAdapter b) {
		this.b = b;
	}
}