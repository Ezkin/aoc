package fr.istic.m2miage.metronome;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class Metronome extends Application {

    /**
     * Method start pour demarrer le metronome.
     * On peut ainsi initialiser le moteur, l'IHM et le controlleur.
     * On regle egalement les proprietes de JavaDX (FXML, Titre, Dimension...)
     * @param primaryStage Stage
     * @throws Exception si une erreur est detectee
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
    	Moteur moteur = new MoteurImpl();
    	IHM ihm = new IHMAdapter();
    	new Controller(moteur, ihm);
    	
    	FXMLLoader loader = new FXMLLoader();
    	
    	loader.setLocation(getClass().getResource("Metronome.fxml"));
    	
    	loader.setController(ihm);
    	Parent root = loader.<Parent>load();
    	
    	primaryStage.setTitle("Metronome");
        primaryStage.setScene(new Scene(root, 394, 255));
        primaryStage.show();
        
    }


    /**
     * Method main pour demarrer l'application
     * @param args String[]
     */
    public static void main(String[] args) {
        launch(args);
    }
}
