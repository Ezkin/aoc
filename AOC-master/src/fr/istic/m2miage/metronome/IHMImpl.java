package fr.istic.m2miage.metronome;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.FillTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class IHMImpl implements Initializable, IHM {

    private SoundEmmitter sound;

	private Controller controller;


	@FXML private Slider slider;
    @FXML private Label label;
    @FXML private Circle l1, l2;

    public IHMImpl() {
    	sound = new SoundEmmitterImpl();
    }

    /**
	 * Method start appelee lors d'un clique sur le bouton start via JavaFX
	 */
    public void start(){
    	controller.start();
    }

    /**
	 * Method stop appelee lors d'un clique sur le bouton start via JavaFX
	 */
    public void stop(){
    	controller.stop();
    }

    /* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#inc()
	 */
    @Override
	public void inc(){
    	controller.inc();
    }

    /* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#dec()
	 */
    @Override
	public void dec(){
    	controller.dec();
    }

    /*
     * Method updateSlider.
     * @see fr.istic.m2miage.metronome.IHM#updateSlider()
     */
    public void updateSlider(){
    	controller.updateSlider((int) slider.getValue());
    }

    /*
     * Method markTempo.
     * @see fr.istic.m2miage.metronome.IHM#markTempo()
     */
    public void markTempo(){
        //animation led
    	FillTransition tt = new FillTransition(Duration.millis(100), l1, Color.GREEN, Color.WHITE);
        tt.setAutoReverse(true);
        tt.play();

        //son
        sound.playSound();
    }

    /* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#markMesure()
	 */
    @Override
	public void markMesure(){
    	FillTransition tt = new FillTransition(Duration.millis(100), l2, Color.RED, Color.WHITE);
        tt.setAutoReverse(true);
        tt.play();
    }
    
    /*
	 * @param tempo
	 * @see fr.istic.m2miage.metronome.IHM#updateTempo(int)
     */
	public void updateTempo(int tempo) {
		label.setText(tempo + "");
		slider.setValue(tempo);
	}
    
    /*
     * Method initialize.
     * @param location URL
     * @param resources ResourceBundle
     * @see javafx.fxml.Initializable#initialize(URL, ResourceBundle)
     */
    @Override
	public void initialize(URL location, ResourceBundle resources) {
		slider.setValue(100);
	}

	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#getSlider()
	 */
	@Override
	public Slider getSlider() {
		return slider;
	}


	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#getLabel()
	 */
	@Override
	public Label getLabel() {
		return label;
	}


	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#getL1()
	 */
	@Override
	public Circle getL1() {
		return l1;
	}


	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#getL2()
	 */
	@Override
	public Circle getL2() {
		return l2;
	}


	public Controller getController() {
		return controller;
	}


	/*
     * Method setController.
     * @param controller Controller
     * @see fr.istic.m2miage.metronome.IHM#setController(Controller)
     */
    @Override
	public void setController(Controller controller) {
		this.controller = controller;
	}

	public SoundEmmitter getSound() {
		return sound;
	}

	public void setSound(SoundEmmitter sound) {
		this.sound = sound;
	}
}
