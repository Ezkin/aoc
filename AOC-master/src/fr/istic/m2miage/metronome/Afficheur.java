/**
 * 
 */
package fr.istic.m2miage.metronome;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public interface Afficheur {
	/**
	 * Method allumerLED pour allumer la LED numLED de l'IHM
	 * @param numLED int entre 1 et 2
	 */
	public void allumerLED(int numLED);
	/**
	 * Method eteindreLED pour éteindre la LED numLED de l'IHM
	 * @param numLED int entre 1 et 2
	 */
	public void eteindreLED(int numLED);
	/**
	 * Method afficherTempo pour afficher le Tempo valeurTempo sur l'IHM
	 * @param valeurTempo entre 30 et 300
	 */
	public void afficherTempo(int valeurTempo);
}
