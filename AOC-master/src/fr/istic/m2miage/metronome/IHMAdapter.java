package fr.istic.m2miage.metronome;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.FillTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 * @author François Esnault, Emmanuel Petit [M2 MIAGE]
 * @date 08/01/2016
 */
public class IHMAdapter implements Initializable, IHM, Clavier, Molette {

	private SoundEmmitter sound;

	private Controller controller;

	@FXML private Button start;
	@FXML private Button stop;
	@FXML private Button inc;
	@FXML private Button dec;
	@FXML private Slider slider;
	@FXML private Label label;
	@FXML private Circle l1, l2;

	/**
	 * Constructeur de l'IHM, on cree un nouveau SoundEmitter pour generer les sons
	 */
	public IHMAdapter() {
		sound = new SoundEmmitterImpl();
	}

	/**
	 * Method start appelee par la commande si elle détecte que le bouton est enfonce
	 */
	public void start(){
		controller.start();
	}

	/**
	 * Method stop appelee par la commande si elle détecte que le bouton est enfonce
	 */
	public void stop(){
		controller.stop();
	}

	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#inc()
	 */
	@Override
	public void inc(){
		controller.inc();
	}

	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#dec()
	 */
	@Override
	public void dec(){
		controller.dec();
	}

	/**
	 * Method updateSlider.
	 * @see fr.istic.m2miage.metronome.IHM#updateSlider()
	 */
	public void updateSlider(){
		controller.updateSlider((int) slider.getValue());
	}

	/**
	 * Method markTempo.
	 * @see fr.istic.m2miage.metronome.IHM#markTempo()
	 */
	public void markTempo(){
		//animation led
		FillTransition tt = new FillTransition(Duration.millis(100), l1, Color.GREEN, Color.WHITE);
		tt.setAutoReverse(true);
		tt.play();

		//son
		sound.playSound();
	}

	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#markMesure()
	 */
	@Override
	public void markMesure(){
		FillTransition tt = new FillTransition(Duration.millis(100), l2, Color.RED, Color.WHITE);
		tt.setAutoReverse(true);
		tt.play();
	}

	/**
	 * @param tempo
	 * @see fr.istic.m2miage.metronome.IHM#updateTempo(int)
	 */
	public void updateTempo(int tempo) {
		label.setText(String.valueOf(tempo));
	}

	/**
	 * Method initialize pour demarrer JavaFX.
	 * On initialise le slider à 100.
	 * On cree les commandes :
	 * 	start
	 * 	stop
	 * 	inc
	 * 	dec
	 * 	updateSlider
	 * @param location URL
	 * @param resources ResourceBundle
	 * @see javafx.fxml.Initializable#initialize(URL, ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		slider.setValue(100);

		CommandControlleur start = new CommandControlleur() {
			private Controller _ctrl;

			@Override
			public void execute() {
				_ctrl.start();
			}

			@Override
			public void setReceiver(Controller controller) {
				_ctrl = controller;
			}
		};

		start.setReceiver(controller);

		new ButtonAdapter(this, 1, start);

		CommandControlleur stop = new CommandControlleur() {
			private Controller _ctrl;

			@Override
			public void execute() {
				_ctrl.stop();
			}

			@Override
			public void setReceiver(Controller controller) {
				_ctrl = controller;
			}
		};

		stop.setReceiver(controller);

		new ButtonAdapter(this, 2, stop);

		CommandControlleur inc = new CommandControlleur() {
			private Controller _ctrl;

			@Override
			public void execute() {
				_ctrl.inc();
			}

			@Override
			public void setReceiver(Controller controller) {
				_ctrl = controller;
			}
		};

		inc.setReceiver(controller);

		new ButtonAdapter(this, 3, inc);

		CommandControlleur dec = new CommandControlleur() {
			private Controller _ctrl;

			@Override
			public void execute() {
				_ctrl.dec();
			}

			@Override
			public void setReceiver(Controller controller) {
				_ctrl = controller;
			}
		};

		dec.setReceiver(controller);

		new ButtonAdapter(this, 4, dec);

		CommandAdapter updateSlider = new CommandAdapter() {
			private IHM _ihm;

			@Override
			public void execute() {
				_ihm.updateSlider();
			}

			@Override
			public void setReciever(IHM ihm) {
				_ihm = ihm;
			}
		};

		updateSlider.setReciever(this);

		new SliderAdapter(this, updateSlider);
	}



	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#getSlider()
	 */
	@Override
	public Slider getSlider() {
		return slider;
	}


	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#getLabel()
	 */
	@Override
	public Label getLabel() {
		return label;
	}


	/* (non-Javadoc)
	 * @see fr.istic.m2miage.metronome.IHMInterface#getL1()
	 */
	@Override
	public Circle getL1() {
		return l1;
	}

	/*
	 * Method getL2.
	 * @return Circle
	 * @see fr.istic.m2miage.metronome.IHM#getL2()
	 */
	@Override
	public Circle getL2() {
		return l2;
	}

	/*
	 * Method setController.
	 * @param controller Controller
	 * @see fr.istic.m2miage.metronome.IHM#setController(Controller)
	 */
	@Override
	public void setController(Controller controller) {
		this.controller = controller;
	}

	/*
	 * Method touchePressee.
	 * @param i int
	 * @return boolean
	 * @see fr.istic.m2miage.metronome.Clavier#touchePressee(int)
	 */
	@Override
	public boolean touchePressee(int i) {
		switch(i) {
		case 1: if(start.isPressed()) return true; break;
		case 2: if(stop.isPressed()) return true; break;
		case 3: if(inc.isPressed()) return true; break;
		case 4: if(dec.isPressed()) return true; break;
		}
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * Method position.
	 * @return float
	 * @see fr.istic.m2miage.metronome.Molette#position()
	 */
	@Override
	public float position() {
		return (float) getSlider().getValue();
	}

}
