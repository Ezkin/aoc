#Metronome

*Projet AOC en deuxième année de Master MIAGE*

*Binome : ESNAULT François, PETIT Emmanuel*

*Encadrant : PLOUZEAU Noël*

###Exécutables 
- [Version 1 : /out/production/ACO_Metronome_V1.jar](out/production/ACO_Metronome_V1.jar)
- [Version 2 : /out/production/ACO_Metronome_V2.jar](out/production/ACO_Metronome_V2.jar)

###Javadoc
- [Rapport Javadoc : /javadoc](https://github.com/fresnault/AOC/tree/master/javadoc)
- La javadoc a été généré à l'aide de l'outil Code Pro Tool.

###Tests
- [Rapport de test : /src/fr.istic.m2miage/tests/Test Results - ACO-Metronome.html](http://htmlpreview.github.io/?https://raw.githubusercontent.com/fresnault/AOC/master/src/fr/istic/m2miage/tests/Test%20Results%20-%20ACO-Metronome.html?token=AEbzBgCgpuilww1xbOi-aNLjDFyX1-yOks5WmmIHwA%3D%3D)
- Les tests ont été réalisé avec JUnit 4 et Mockito. L'ensemble des implémentations du projet ont été testées.

###Introduction

Le métronome émet des signaux dans le temps en fonction des paramètres tempo et mesure. Un flash lumineux et un signal sonore seront émis pour chaque temps par mesure. Enfin, une seconde led va émettre un signal lumineux lors des mesures.

![image_metronome](docs/image_metronome.png)

Les paramètres de tempo et mesure sont configurables via un clavier de commandes.

###Liste des commandes :

 - Start : Active les signaux sonores/lumineux
 - Stop : Désactive les signaux
 - Inc : Augmente le nombre de temps dans une mesure d'une unité
 - Dec : Diminue le nombre de temps dans une mesure d'une unité

###Préparation

La première phase de la réalisation a été la conception de l'application et des différentes fonctionnalités sous forme de diagrammes. Avec le support des travaux dirigés, nous avons construits les diagrammes de classes reprenant la mise en œuvre des différents patrons de conception au sein de l'application. Ces diagrammes nous ont permis de générer les premières classes avec les différents liens d'interconnexion via l’outil de génération de code Java de StarUML.

###Version 1

La première version du métronome est centrée sur la construction d'un moteur capable d’émettre des signaux à intervalles réguliers. Ce moteur traite deux informations à savoir le tempo et la mesure. Ainsi, il détermine la fréquence de clignotement des leds qui marquent ces temps. Ces deux paramètres sont modifiables par l'utilisateur via l'IHM qui sert de point d'entrée. A tout moment ce moteur peut être stoppé et relancé et les valeurs de tempo et mesure peuvent être changées.

Dès le début, nous avons décidé d'utiliser JavaFX pour réaliser notre interface et de profiter des nombreuses fonctionnalités qu'il offre notamment l'attachement de fonctions au éléments tel que les boutons et la molette. Ainsi, l’interaction avec ces éléments déclenche automatiquement l'appel de la méthode liée et dont le code est défini dans le contrôleur. Un autre avantage de cette méthode est que l'on se passe de la déclaration de listener qui peuvent très vite surcharger le code de l'application.
![image_ihmimpl_controller](docs/image_ihmimpl_controller.jpg)

Pour faire communiquer les différents éléments, nous avons mis en œuvre deux patrons de conception : Command et Observer. Cette combinaison nous permet d'utiliser le mécanisme de commandes afin de notifier les observeurs, rendant le code beaucoup plus lisible et modulable. 
![image_command_observer](docs/image_command_observer.jpg)
Pour ce faire, nous avons défini dans le moteur (qui joue le rôle d'invoker) une liste de commandes (avec une HashMap qui permet de les retrouver simplement). Les commandes sont fournies par le contrôleur lors de l'initialisation. Cette liaison va permette au moteur de notifier le contrôleur d'un changement d'état, de faire clignoter les led et de jouer du son.

![image_diag_sequence](docs/image_diag_sequence.jpg)

Afin de réaliser une action périodiquement sur le moteur, nous avons implémenté une horloge qui par un mécanisme de commandes notifie à intervalles réguliers l'objet qui lui est lié, dans notre cas, le moteur.
Pour se faire, on calcule la durée d'un "tick" en fonction du tempo et de la mesure. Afin de garantir le plus de précision, l'horloge n’exécute qu'une seule commande sur le moteur et c'est lui qui décide s'il doit marquer une mesure ou un temps. Le moteur devient alors un Invoker si on considère le patron de conception commande. Etant donné que nous avons dans notre systèmes deux receivers possibles pour les commandes, nous avons créé une interface SuperCommand qui est implémentée par les classes CommandControlleur et CommandMoteur et chacune définit le receiver correspondant.

![image_command](docs/image_command.jpg)

![image_ihm_sequence.png](docs/image_ihm_sequence.png)

En règle générale, le développement s'est bien passé et les quelques obstacles rencontrés ont pu être contournés sans difficulté mais rien de bloquant.
 
###Version 2

Une fois le métronome fonctionnel, il faut se séparer de l'IHM virtuelle mais pas complètement.
Cette fois, on se charge de lire l'état des boutons et de notifier les changements qui correspondent à des interactions de l'utilisateur. La consigne étant que le moteur doit rester inchangé pour cette version. 
Le point clé de cette version était la mise en œuvre du patron de conception Adapter afin de fournir une interface permettant d'accéder au système existant sans le modifier et tout de même réaliser les mêmes actions. Rapidement nous nous sommes rendus compte que la modification du contrôleur était nécessaire. Donc nous avons décidé de revenir à la version 1 afin de rajouter une interface supplémentaire entre le contrôleur et l'IHM.

Slider
![image_controles_slider](docs/image_controles_slider.jpg)

Bouttons
![image_controles_button](docs/image_controles_button.jpg)

En premier lieu, il a fallu modifier le comportement des boutons et de la molette (appelons-les : les contrôles). Maintenant, ceux ci ne communiquent plus directement, il faut lire en permanence leur état. Pour cela, nous avons défini un IHMAdapter en charge de gérer les changements d'états des contrôles et de diffuser l'information.
De plus, il joue le rôle d'Adapter car il permet d'adapter ce nouveau mode de fonctionnement sans changer le rester du code à savoir le contrôleur et le moteur.

![image_adapter_command](docs/image_adapter_command.jpg)

Pour ce faire, l'IHMAdapter met en œuvre le patron de conception Command dans lequel il joue le rôle du receiver. Ainsi il initialise des commandes qui seront appelées si l'état des contrôles correspondant est changé. Cet appel est géré par un mécanisme de Poll. En effet, chaque contrôle instancie une commande Poll dont il est le receiver. Cette commande est ensuite passée à l'horloge qui va périodiquement, dans notre cas toutes les 100 milisecondes, appeler la méthode poll du contrôle correspondant. A chaque fois, celui ci va vérifier si l'état courant est différent de l'état précédent. Si c'est le cas, il notifie le Clavier (qu’il a reçu lors de l’initialisation) de se changement qui correspond alors à une interaction utilisateur.

![image_horloge_sequence.png](docs/image_horloge_sequence.png)

Maintenant, nous n'avons plus qu'a brancher l'IHM adaptée sur le contrôleur et faire en sorte que les changements d'états des contrôles soient transmis au contrôleur.


### Et ensuite ?

Il serait judicieux de tester le projet sur un vrai métronome afin de savoir si l'adaptateur joue bien son rôle.
